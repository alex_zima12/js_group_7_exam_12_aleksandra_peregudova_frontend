import React from 'react';
import {useSelector} from "react-redux";
import Routes from "./Routes";
import Toolbar from "./components/Navigation/Toolbar/Toolbar";

const App = () => {
  const user = useSelector(state => state.users.user);
  return (
    <>
      <Toolbar user={user} />
      <main>
          <Routes user={user} />
      </main>
    </>
  )
};

export default App;
