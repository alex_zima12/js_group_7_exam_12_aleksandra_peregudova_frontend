import React from "react";
import {useDispatch} from "react-redux";
import {logoutUser} from "../../../store/actions/usersActions";
import {NavLink} from "react-router-dom";

const UserMenu = ({user}) => {
    const dispatch = useDispatch();

    const logout = () => {
        dispatch(logoutUser());
    };

    return user && (
            <div>
                <NavLink className="font-weight-bold text-light m-3" to={"/users/" + user.user._id}>
                    Hello, {user.user.displayName || user.user.username}!</NavLink>
                <button className="font-weight-bold m-3" onClick={logout}>Logout</button>
            </div>
);
};

export default UserMenu;