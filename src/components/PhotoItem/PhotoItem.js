import React from 'react';
import {apiURL} from "../../constants";
import {NavLink} from 'react-router-dom';
import defaultImage from "../../assets/images/SD-default-image.png";
import {useDispatch} from "react-redux";
import {deletePhoto} from "../../store/actions/photosActions";

const myStyle = {
    width: "300px",
    height:"300px"
};

const PhotoItem = (props) => {
    const dispatch = useDispatch();

    const  deleteItem = () => {
        dispatch(deletePhoto(props.photo._id));
    };

    let cardImage = defaultImage;
    if (props.photo.image) {
        cardImage =  apiURL + "/uploads/" + props.photo.image;
    }

    let button;
    if (props.user && props.user._id === props.photo.user._id) {
        button = <button className="text-danger mt-3" onClick={deleteItem}> Delete</button>
    }

    return props && (
        <div className="col-md-4">
            <div className="card d-flex col-sm m-3 p-3">
                <h5 className="card-title  flex-md-grow-1 ">{props.photo.title}</h5>
                <img src={cardImage}
                     className="card-img-top flex-md-grow-1 p-3"
                     alt="cocktail"
                     style={myStyle}
                />
                {button}
                <NavLink className="font-weight-bold text-decoration-none  ml-3" to={"/users/" + props.photo.user._id}>
                    Photo by : {props.photo.user.displayName}</NavLink>
            </div>
        </div>
    );
};

export default PhotoItem;