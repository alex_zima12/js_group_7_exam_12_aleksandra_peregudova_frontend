import React, {useState} from "react";
import {useDispatch,useSelector} from "react-redux";
import {createNewPhoto} from "../../store/actions/photosActions";

const NewPhotos = (props) => {
    const dispatch = useDispatch();
    const error = useSelector(state => state.photos.error);

    const [state, setState] = useState({
        title: '',
        image: '',
    });

    const submitFormHandler = e => {
        e.preventDefault();
        let formData = new FormData();
        Object.keys(state).forEach(key => {
            formData.append(key, state[key]);
        });
        dispatch(createNewPhoto(formData))
    };
    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };
    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setState(prevState => ({...prevState, [name]: file}));
    };

    let errorMes;
    if (error){
        errorMes = <div className="text-danger mb-3">{error.error}</div>
    }

    return (
        <div className="container bg-gradient-info">
            <h1 className="text-primary mt-3">Add new Photo</h1>
            <form
                className="p-5"
                onSubmit={submitFormHandler}
            >
                <div className="input-group mb-3">
                    <div className="input-group-prepend">
                        <span className="input-group-text" id="basic-addon1">Title</span>
                    </div>
                    <input type="text"
                           className="form-control p-3"
                           placeholder="Enter title that reflects the idea of your photo"
                           value={state.title}
                           onChange={inputChangeHandler}
                           name="title"
                           required/>
                </div>

                <div className="input-group mb-3 ">
                    <label htmlFor="image">Add file</label>
                    <input
                        type="file"
                        name="image"
                        className="form-control-file"
                        onChange={fileChangeHandler}
                        required
                    />
                </div>
                <div className="border-danger">
                    {errorMes}
                </div>
                <button
                    type="submit"
                    className="btn btn-info"
                >Add new photo
                </button>
            </form>
        </div>
    );
};

export default NewPhotos;