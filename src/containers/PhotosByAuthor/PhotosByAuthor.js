import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchAuthorPhotos} from "../../store/actions/photosActions";
import PhotoItem from "../../components/PhotoItem/PhotoItem";
import {NavLink} from "react-router-dom";
import defaultImage from "../../assets/images/SD-default-image.png";
import {apiURL} from "../../constants";
import Modal from "../../UI/Modal/Modal";

const myStyle = {
    width: "100%"
};

const PhotosByAuthor = (props) => {

    const dispatch = useDispatch();
    const photos = useSelector(state => state.photos.authorPhotos);
    const user = useSelector(state => state.users.user);
    const authorId = props.match.params.id

    const [showing, setShowing] = useState({
        modal: false,
        selectedPhoto: ''
    });

    useEffect(() => {
        dispatch(fetchAuthorPhotos(authorId));
    }, [dispatch, authorId]);

    const showModal = image => {
        setShowing(prevState => {
            return {
                ...prevState,
                modal: true,
                selectedPhoto: image
            };
        });
    };

    const closeModal = () => {
        setShowing(prevState => {
            return {
                ...prevState,
                modal: false
            }
        });
    };

    let cardImage = defaultImage;
    if (showing.selectedPhoto) {
        cardImage = apiURL + "/uploads/" + showing.selectedPhoto;
    }

    return photos && (
        <div className="container">
            {user && authorId === user._id ?
                <div className="m-3">
                    <h1 className="text-center text-primary">Your Photo Gallery</h1>
                    <NavLink className="btn btn-primary font-weight-bold  m-3" to={"/photos/new"}>Add New
                        Photo</NavLink>
                </div>
                : <h1 className="text-center text-primary m-3">Photo Gallery</h1>
            }

            <Modal show={showing.modal} closed={closeModal}>
                <img style={myStyle} src={cardImage} alt="gallery"/>
            </Modal>

            <div className="row">
                {user ?
                    photos.map(photo => {
                        return <PhotoItem
                            key={photo._id}
                            photo={photo}
                            user={user}
                            showPhoto={() => showModal(photo.image)
                            }
                        />
                    }) : photos.map(photo => {
                        return <PhotoItem
                            key={photo._id}
                            photo={photo}
                            showPhoto={() => showModal(photo.image)
                            }
                        />
                    })}
            </div>
        </div>

    );
};

export default PhotosByAuthor;