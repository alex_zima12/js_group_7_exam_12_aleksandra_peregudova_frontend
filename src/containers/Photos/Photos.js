import React, {useState, useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchPhotos} from "../../store/actions/photosActions";
import PhotoItem from "../../components/PhotoItem/PhotoItem";
import Modal from "../../UI/Modal/Modal";
import {apiURL} from "../../constants";
import defaultImage from "../../assets/images/SD-default-image.png";

     const myStyle = {
         width: "100%"
     };

const Photos = () => {
    const dispatch = useDispatch();
    const photos = useSelector(state => state.photos.photos);
    // const error = useSelector(state => state.artists.error);

    const [showing, setShowing] = useState({
        modal: false,
        selectedPhoto: ''
    });

    const showModal = image => {
        setShowing(prevState => {
            return {...prevState,
                modal: true,
                selectedPhoto: image};
        });
    };

    const closeModal = () => {
        setShowing(prevState => {
            return {...prevState,
                modal: false }
        });
    };

    useEffect(() => {
        dispatch(fetchPhotos());
    }, [dispatch]);

    let cardImage = defaultImage;
    if (showing.selectedPhoto) {
        console.log(showing.selectedPhoto.image, "showing.selectedPhoto.image");
        cardImage =  apiURL + "/uploads/" + showing.selectedPhoto;
    }

    return (
        <div className="container">
            <h1>All photos</h1>
            <Modal show={showing.modal} closed={closeModal}>
                    <img style={myStyle} src={cardImage} alt="gallery"/>
            </Modal>
            <div className="row">
                {photos.map(photo => {
                    return <PhotoItem
                        key={photo._id}
                        photo={photo}
                        showPhoto={() => showModal(photo.image)}
                    />
                })}
            </div>
        </div>

    );
};

export default Photos;