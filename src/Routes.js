import React from "react";
import {Redirect, Route, Switch} from "react-router-dom";

import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Photos from "./containers/Photos/Photos";
import NewPhotos from "./containers/NewPhoto/NewPhoto";
import PhotosByAuthor from "./containers/PhotosByAuthor/PhotosByAuthor";

const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
  return isAllowed ?
    <Route {...props} /> :
    <Redirect to={redirectTo} />;
};

const Routes = ({user}) => {
  return (
    <Switch>
        <Route
            path="/"
            exact
            component={Photos}
        />
      <Route
        path="/users/:id"
        exact
        component={PhotosByAuthor}
      />
        <ProtectedRoute
            path="/photos/new"
            exact
            component={NewPhotos}
            isAllowed={user}
            redirectTo="/login"
        />
      <ProtectedRoute
        path="/register"
        exact
        component={Register}
        isAllowed={!user}
        redirectTo="/"
      />
      <ProtectedRoute
        path="/login"
        exact
        component={Login}
        isAllowed={!user}
        redirectTo="/"
      />
    </Switch>
  );
};

export default Routes;