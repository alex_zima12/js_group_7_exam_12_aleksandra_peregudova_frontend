import {
    FETCH_PHOTOS_SUCCESS,
    CREATE_PHOTO_ERROR,
    FETCH_AUTHOR_PHOTO_SUCCESS,
    DELETE_PHOTO_SUCCESS
} from "../actions/actionTypes";

const initialState = {
    photos: [],
    authorPhotos: [],
    error: null
};

const photosReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_AUTHOR_PHOTO_SUCCESS:
            return {...state, authorPhotos: action.photos};
        case FETCH_PHOTOS_SUCCESS:
            return {...state, photos: action.photos};
        case DELETE_PHOTO_SUCCESS:
            const newAuthorPhoto = [...state.authorPhotos];
            const id = newAuthorPhoto.findIndex(p => p.id === action.id);
            newAuthorPhoto.splice(id, 1);
            return {...state, authorPhotos: [...newAuthorPhoto]};
        case CREATE_PHOTO_ERROR:
            return {...state, error: action.error};
        default:
            return state
    }
};

export default photosReducer;
