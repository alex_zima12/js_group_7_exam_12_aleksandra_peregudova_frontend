import axios from "../../axiosApi";
import {push} from "connected-react-router";
import {
    FETCH_PHOTOS_SUCCESS,
    CREATE_PHOTO_SUCCESS,
    CREATE_PHOTO_ERROR,
    FETCH_AUTHOR_PHOTO_SUCCESS,
    DELETE_PHOTO_SUCCESS
} from "./actionTypes";

const fetchPhotosSuccess = photos => {
    return {type: FETCH_PHOTOS_SUCCESS, photos};
};

export const fetchPhotos = () => {
    return async dispatch => {
        try {
            await axios.get("/photos").then(response => {
                dispatch(fetchPhotosSuccess(response.data));
            });
        } catch (e) {
            console.log(e);
        }
    };
};

const fetchAuthorPhotosSuccess = photos => {

    return {type: FETCH_AUTHOR_PHOTO_SUCCESS, photos};
};

export const fetchAuthorPhotos = (id) => {

    return  async dispatch => {
        try {
            await axios.get("/photos?user=" + id).then(response => {
                dispatch(fetchAuthorPhotosSuccess(response.data));
            });
        } catch (e) {
            console.log(e);
        }
    };
};

const createPhotoError = (error) => {
    return {type: CREATE_PHOTO_ERROR, error}
};

export const createNewPhoto = data => {
    return async dispatch => {
        try {
            await axios.post('/photos', data);
            dispatch({type: CREATE_PHOTO_SUCCESS});
            dispatch(push("/"));
        } catch (e) {
            alert(e.response.data.message)
            dispatch(createPhotoError(e.response.data.error));
        }
    };
}

const deletePhotoSuccess = (id) => {
    return {type: DELETE_PHOTO_SUCCESS, id}
};

export const deletePhoto = id => {
    return async dispatch => {
        try {
            const response = await axios.delete('/photos/' + id);
            alert(response.data.message)
            dispatch(deletePhotoSuccess(id));
        } catch (e) {
            console.log(e);
        }
    };
}